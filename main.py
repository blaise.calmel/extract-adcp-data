import copy

import pandas as pd
import open_functions as of
from Classes.Measurement import Measurement
import tkinter as tk
from tkinter import ttk
from threading import Thread
import datetime
import numpy as np
import string
import re
from copy import deepcopy


class Qrame:
    def __init__(self):
        self.data_brm = None
        self.root = tk.Tk()
        self.root.geometry('250x585')
        self.root.resizable(0, 1)
        self.root.title('QRAME')  # QRevint ADCP Massive Extraction
        self.root.wm_iconbitmap(r'QRAME.ico')
        # self.root.wm_attributes("-topmost")
        self.root.bind("<Return>", lambda event: Thread(target=self.run).start())

        self.choice1 = tk.BooleanVar(value=True)
        self.choice2 = tk.BooleanVar(value=True)
        self.choice3 = tk.BooleanVar(value=True)
        self.choice3b = tk.BooleanVar(value=True)
        default_text3 = "Code station HYDRO"
        self.text1b = tk.StringVar(value=1)
        self.text1c = tk.StringVar(value=0)
        self.text3 = tk.StringVar(value=default_text3)
        self.nav_menu4 = tk.StringVar(self.root)
        self.extrap_menu5 = tk.StringVar(self.root)
        self.choice5 = tk.BooleanVar(value=False)
        self.text5 = tk.StringVar(value="0.1667")

        self.choice6 = tk.BooleanVar(value=False)

        # Import ADCP data
        self.label0 = tk.Label(self.root, text="ADCP folder: ***", justify=tk.LEFT)
        self.label0.pack(side=tk.TOP, anchor=tk.W, padx=15)
        self.import_button = tk.Button(self.root, text='Select ADCP data',
                                       command=lambda: Thread(target=self.import_data).start(),
                                       padx=50, pady=4, height=1, width=10)
        self.import_button.pack()

        # Import Barème HYDRO 2 data
        self.label1 = tk.Label(self.root, text="HYDRO2 file: ***",
                               justify='left')
        self.label1.pack(side=tk.TOP, anchor=tk.W, padx=15)
        self.brm_button = tk.Button(self.root, text='Select HYDRO2 file',
                                    command=lambda: Thread(target=self.get_brm_export).start(),
                                    padx=50, pady=4, height=1, width=10)
        self.brm_button.pack()

        self.label1b = tk.Label(self.root, text="Time delta in hours (integer)", justify='left')
        self.label1b.pack(side=tk.TOP, anchor=tk.W, padx=15)
        self.Entry_1b = tk.Entry(self.root, textvariable=self.text1b)
        self.Entry_1b.pack(anchor=tk.W, padx=35, ipadx=35)

        self.label1c = tk.Label(self.root, text="Time difference with Bareme (integer)", justify='left')
        self.label1c.pack(side=tk.TOP, anchor=tk.W, padx=15)
        self.Entry_1c = tk.Entry(self.root, textvariable=self.text1c)
        self.Entry_1c.pack(anchor=tk.W, padx=35, ipadx=35)

        # .dat data with entry box for Bareme station's name
        def some_callback(event):
            if self.text3.get() == default_text3:
                self.Entry_3.delete(0, "end")
            return None

        # Entry text
        self.Entry_3 = tk.Entry(self.root, textvariable=self.text3)
        # self.Entry_3.insert(0, self.text3.get())
        self.Entry_3.bind("<Button-1>", some_callback)
        # Create checkbutton and connect it to entry
        self.Checkbutton_3 = tk.Checkbutton(self.root, text='Save as .dat (Barème)', variable=self.choice3,
                                            command=lambda e=[self.Entry_3],
                                                           v=self.choice3: self.naccheck(e, v))
        # Position checkbutton and entrybox
        self.Checkbutton_3.pack(side=tk.TOP, anchor=tk.W, ipadx=15)
        self.Entry_3.pack(anchor=tk.W, padx=35, ipadx=35)

        # .csv data
        self.Checkbutton_1 = tk.Checkbutton(self.root, text='Save as .csv (BaRatinAGE)', variable=self.choice1)
        self.Checkbutton_1.pack(side=tk.TOP, anchor=tk.W, ipadx=15)
        # .BAT data
        self.Checkbutton_2 = tk.Checkbutton(self.root, text='Save as .BAD (BaRatinAGE)', variable=self.choice2)
        self.Checkbutton_2.pack(side=tk.TOP, anchor=tk.W, ipadx=15)
        # General data
        self.Checkbutton_3b = tk.Checkbutton(self.root, text='Save as .csv (general export)', variable=self.choice3b)
        # self.Checkbutton_3b.pack(side=tk.TOP, anchor=tk.W, ipadx=15)

        # Chose navigation reference
        self.label4 = tk.Label(self.root, text="Navigation reference")
        self.label4.pack(side=tk.TOP, anchor=tk.W, padx=15)
        self.nav_menu4.set("Default")  # default value
        self.Option_nav4 = tk.OptionMenu(self.root, self.nav_menu4, "Default", "Bottom track")
        self.Option_nav4.pack(anchor=tk.W, padx=35, ipadx=35)

        # Chose extrap
        self.label5 = tk.Label(self.root, text="Extrapolation law")
        self.label5.pack(side=tk.TOP, anchor=tk.W, padx=15)
        self.extrap_menu5.set("Default")  # default value
        # Chose exp value
        self.Entry_5 = tk.Entry(self.root, textvariable=self.text5)
        # Chose if personnalize exp
        self.Checkbutton_5 = tk.Checkbutton(self.root, text='Personnalize exp.', variable=self.choice5,
                                            command=lambda e=[self.Entry_5], v=self.choice5: self.naccheck(e, v))
        # Chose law
        self.Option_extrap5 = tk.OptionMenu(self.root, self.extrap_menu5, "Default", "Power", "CNS", "3-points",
                                            command=lambda e=self.Checkbutton_5, v=self.extrap_menu5: self.nanmenu(e,
                                                                                                                   v))
        self.Option_extrap5.pack(anchor=tk.W, padx=35, ipadx=35)
        self.Checkbutton_5.pack(side=tk.TOP, anchor=tk.W, ipadx=15)
        self.Entry_5.pack(anchor=tk.W, padx=35, ipadx=35)

        # No moving-bed detected
        self.Checkbutton_6 = tk.Checkbutton(self.root, text='No moving-bed detected', variable=self.choice6)
        self.Checkbutton_6.pack(side=tk.TOP, anchor=tk.W, ipadx=15)

        # Progress bar
        self.pb = ttk.Progressbar(self.root, orient='horizontal', length=100)
        self.pb.pack(side=tk.TOP, anchor=tk.W, padx=10, ipadx=200)
        self.value_label = ttk.Label(self.root, text=self.update_progress_label())
        self.value_label.pack()

        # Run Button
        self.run_button = tk.Button(self.root, text='Run [Return]', command=lambda: Thread(target=self.run).start(),
                                    padx=50, pady=10, height=1, width=12)
        self.run_button.pack()

        # Close button
        self.close_button = tk.Button(self.root, text='Close', command=self.close, padx=50, pady=10,
                                      height=1, width=12)
        self.close_button.pack()
        self.disable_all(way=True, import_data=False)

        self.root.protocol('WM_DELETE_WINDOW', self.close)
        self.root.mainloop()

    def close(self):
        self.root.destroy()

    def progress(self, value):
        self.pb['value'] = value
        self.value_label['text'] = self.update_progress_label()

    def update_progress_label(self, run=True):
        if run:
            return f"Current Progress: {int(self.pb['value'])}%"
        else:
            self.pb['value'] = 0
            self.pb.update_idletasks()
            return f'{self.name_folder} extraction completed'

    def naccheck(self, entry, var):
        # if self.choice3.get() == 0:
        if var.get() == 0:
            for e in entry:
                e.configure(state='disabled')
        else:
            for e in entry:
                e.configure(state='normal')

    def nanmenu(self, entry, var):
        if var.get() == "Default":
            self.Checkbutton_5["state"] = "disabled"
            self.Entry_5.configure(state='disabled')
        else:
            self.Checkbutton_5["state"] = "normal"
            self.naccheck([self.Entry_5], self.choice5)

    def import_data(self):
        self.import_button["state"] = "disabled"
        self.run_button["state"] = "disabled"
        path_folder, path_meas, type_meas, name_meas, no_adcp = of.select_directory()
        if type_meas is not None:
            if len(type_meas) == 0:
                tk.messagebox.showerror(title='Wrong folder', message='Invalid folder selected.')
                self.import_button["state"] = "normal"
                return
            else:
                if len(no_adcp) > 0:
                    print('errors in folders')
                    no_adcp_str = ', '.join(no_adcp)
                    tk.messagebox.showwarning(title='No ADCP measurement',
                                              message=f'No ADCP measure file found in the folders : {no_adcp_str}')

                self.disable_all(way=False)
                self.path_folder = path_folder
                self.path_meas = path_meas
                self.type_meas = type_meas
                self.name_meas = name_meas
                self.name_folder = self.path_folder.split('\\')[-1]
                self.label0.config(wraplength=self.root.winfo_width())
                self.label0.config(text=f'ADCP folder: {self.name_folder}')
                self.label0.config(wraplength=self.label0.winfo_width())
                self.run_button["state"] = "normal"
        self.import_button["state"] = "normal"

    def get_brm_export(self):
        self.data_brm = None
        # Select a file path
        path_brm = of.select_file()
        print(path_brm)
        if path_brm:
            try:
                with open(path_brm) as f:
                    lines = f.readlines()

                row = []
                for line in lines[2:-1]:
                    row.append(line.rstrip('\n').split(';')[:23])

                data_brm = pd.DataFrame(row, columns=['C', 'JGG', 'station_name',
                                                      'num', 'date', 'start',
                                                      'end', 'cote', 'cote_start',
                                                      'cote_end', 'code_station',
                                                      'debit', 'incertitude',
                                                      'distance_station', 'sect_mouillee',
                                                      'peri_mouille', 'larg_miroir',
                                                      'vitesse_moyenne', 'vitesse_maxi',
                                                      'vitesse_maxi_surface', 'deniv',
                                                      'commentaire', 'mode']
                                        )
                data_brm = data_brm[data_brm['mode'] == 'PC']
                data_brm['time_start'] = pd.to_datetime(data_brm['date'] + ' ' + data_brm['start'],
                                                        format='%Y/%m/%d %H:%M').astype('datetime64[ns]')
                data_brm['time_end'] = pd.to_datetime(data_brm['date'] + ' ' + data_brm['end'],
                                                      format='%Y/%m/%d %H:%M').astype('datetime64[ns]')
                data_brm['time_mid'] = [data_brm.iloc[i]['time_start'] + (data_brm.iloc[i]['time_end'] -
                                                                          data_brm.iloc[i]['time_start']) / 2 for i in
                                        range(len(data_brm['time_start']))]
                self.data_brm = data_brm
                name_brm = path_brm.split('/')[-1]
                self.label1.config(wraplength=self.root.winfo_width())
                self.label1.config(text=f'HYDRO2 file: {name_brm}')
                self.label1.config(wraplength=self.label1.winfo_width())
                self.Entry_1b.configure(state='normal')
                self.Entry_1c.configure(state='normal')
            except:
                tk.messagebox.showerror(title='Invalid Barème import', message='Please enter a valid Barème import.')
        else:
            self.data_brm = None
            self.Entry_1b.configure(state='disabled')
            self.Entry_1c.configure(state='disabled')
            self.label1.config(text="HYDRO2 file: ***")

    def disable_all(self, way=True, import_data=True):
        # Able/Disable button
        if way:
            if import_data:
                self.import_button["state"] = "disabled"
            self.Checkbutton_3["state"] = "disabled"
            self.Entry_3.configure(state='disabled')
            self.brm_button["state"] = "disabled"
            self.Entry_1b.configure(state="disabled")
            self.Entry_1c.configure(state="disabled")
            self.Checkbutton_1["state"] = "disabled"
            self.Checkbutton_2["state"] = "disabled"
            self.Checkbutton_3b["state"] = "disabled"
            self.Option_nav4["state"] = "disabled"
            self.Option_extrap5["state"] = "disabled"
            self.Checkbutton_5["state"] = "disabled"
            self.Entry_5.configure(state='disabled')
            self.Checkbutton_6["state"] = "disabled"
            self.run_button["state"] = "disabled"
            self.close_button["state"] = "disabled"
        else:
            if import_data:
                self.import_button["state"] = "normal"
            self.Checkbutton_3["state"] = "normal"
            self.naccheck([self.Entry_3], self.choice3)
            self.brm_button["state"] = "normal"
            if self.data_brm is not None:
                self.Entry_1b.configure(state='normal')
                self.Entry_1c.configure(state='normal')
            self.Checkbutton_1["state"] = "normal"
            self.Checkbutton_2["state"] = "normal"
            self.Checkbutton_3b["state"] = "normal"
            self.Option_nav4["state"] = "normal"
            self.Option_extrap5["state"] = "normal"
            self.nanmenu(self.Checkbutton_5, self.extrap_menu5)
            self.Checkbutton_6["state"] = "normal"
            self.run_button["state"] = "normal"
            self.close_button["state"] = "normal"

    def run(self):
        print(self.run_button["state"])
        if self.run_button["state"] == "disabled":
            return
        self.disable_all(way=True)

        # Default value
        nav_ref = None
        fit_method = 'Automatic'
        top = None
        bot = None
        exponent = None
        self.name_folder = self.path_folder.split('\\')[-1]
        extrap_str = 'automatic'
        exp_str = ''
        data_brm = deepcopy(self.data_brm)

        # Get input
        save_as_dat = self.choice3.get()
        name_station = self.text3.get()
        try:
            delta_time = int(self.text1b.get())
        except ValueError:
            tk.messagebox.showerror(title='Invalid time delta', message='Please enter correct time delta as integer.')
            self.disable_all(way=False)
            return
        try:
            lag_time = int(self.text1c.get())
        except ValueError:
            tk.messagebox.showerror(title='Invalid lag time',
                                    message='Please enter correct time difference as integer.')
            self.disable_all(way=False)
            return

        save_as_csv = self.choice1.get()
        save_as_bad = self.choice2.get()
        # save_general = self.choice3b.get()
        input_nav = self.nav_menu4.get()
        input_extrap_type = self.extrap_menu5.get()
        input_extrap_mode = self.choice5.get()
        input_extrap_exp = self.text5.get()
        no_bt = self.choice6.get()

        if input_nav == 'Bottom track':
            nav_ref = 'BT'
        if input_extrap_type != 'Default':
            if input_extrap_type == 'Power':
                top = 'Power'
                bot = 'Power'
                fit_method = 'Manual'
                extrap_str = 'PP'
            elif input_extrap_type == 'CNS':
                top = 'Constant'
                bot = 'No Slip'
                fit_method = 'Manual'
                extrap_str = 'CNS'
            elif input_extrap_type == '3-points':
                top = '3-Point'
                bot = 'No Slip'
                fit_method = 'Manual'
                extrap_str = '3P-NS'
            if input_extrap_mode:
                exponent = input_extrap_exp

        # Check if Code station is correct
        if save_as_dat and name_station in ['Code station HYDRO', '']:
            tk.messagebox.showerror(title='Invalid code station', message='Please enter a valid station code.')
            self.disable_all(way=False)
            return

        if save_as_dat and self.data_brm is None:
            res = tk.messagebox.askquestion('No Barème import',
                                            'You didn\'t select a Barème import, stages will be set '
                                            'at -999m or -32m. Do you want to continue ?')
            if res != 'yes':
                self.disable_all(way=False)
                return

        # Check if exponent is correct
        if exponent is not None:
            exponent = (exponent.replace(',', '.'))
            try:
                exponent = float(exponent)
                exp_str = ' ' + str(np.round(exponent, 2))
            except:
                tk.messagebox.showerror(title='Invalid exponent', message='Please enter a valid exponent.')
                self.disable_all(way=False)
                return
            if exponent > 1 or exponent < 0:
                tk.messagebox.showerror(title='Invalid exponent', message='Exponent value is wrong, please enter an \n '
                                                                          'exponent value between 0 and 1.')
                self.disable_all(way=False)
                return

        try:
            self.pb['value'] = 0
            self.pb.update_idletasks()
            self.value_label['text'] = self.update_progress_label()

            if save_as_csv or save_as_bad or save_as_dat:
                uh_list = []
                q_list = []
                uq_list = []
                date_list = []
                date_day_list = []
                date_hour_start = []
                date_hour_end = []

                width_list = []
                area_list = []

                default_checked = []
                current_checked = []
                error_meas = []

                for id_meas in range(len(self.path_meas)):
                    print(f"==================== {id_meas} ====================")
                    print(self.path_meas[id_meas])
                    print(self.type_meas[id_meas])
                    self.pb['value'] = 100 * (id_meas + 1) / len(self.path_meas)
                    self.pb.update_idletasks()
                    self.value_label['text'] = self.update_progress_label()
                    valid_meas = False
                    try:
                        # Open measurement
                        meas, checked_transect, navigation_reference = of.open_measurement(self.path_meas[id_meas],
                                                                                           self.type_meas[id_meas],
                                                                                           use_weighted=True,
                                                                                           navigation_reference=nav_ref,
                                                                                           run_oursin=True)
                        valid_meas = True
                    except:
                        error_meas.append(self.name_meas[id_meas])

                    if valid_meas:
                        default_checked.append(str(len(checked_transect)))
                        # Remove transect with Delta Q > 40%
                        discharge = meas.mean_discharges(meas)

                        remove = []
                        for transect_id in checked_transect:
                            per_diff = np.abs(((meas.discharge[transect_id].total - discharge['total_mean']) /
                                               discharge['total_mean']) * 100)
                            if per_diff > 40 and len(checked_transect) > 4:
                                remove.append(checked_transect.index(transect_id))

                        if remove:
                            checked_transect = [elem for elem in checked_transect if elem not in remove]
                            meas, checked_transect, navigation_reference = \
                                of.new_settings(meas, checked_transect_user=checked_transect)
                        current_checked.append(str(len(checked_transect)))

                        # Change extrapolation method
                        if fit_method == 'Manual':
                            meas.extrap_fit.change_fit_method(meas.transects, 'Manual', len(meas.transects), top, bot,
                                                              exponent)
                            print(f'Top : {top}, bot : {bot}, exp : {exponent}')
                            settings = meas.current_settings()
                            meas.apply_settings(settings)
                        # No moving-bed obeserved
                        if no_bt and meas.current_settings()['NavRef'] == 'bt_vel':
                            meas.observed_no_moving_bed = True
                            meas.oursin.compute_oursin(meas)

                        # Date data
                        date_value = meas.transects[checked_transect[0]].date_time.start_serial_time
                        date_lag = datetime.datetime.utcfromtimestamp(date_value) + datetime.timedelta(hours=lag_time)
                        date_list.append(datetime.datetime.strftime(date_lag, '%Y-%m-%d %H:%M'))
                        date_day_list.append(datetime.datetime.strftime(date_lag, '%Y%m%d'))
                        date_hour_start.append(datetime.datetime.strftime(date_lag, '%H:%M'))

                        date_hour_end.append(
                            datetime.datetime.strftime(datetime.datetime.utcfromtimestamp(
                                meas.transects[checked_transect[-1]].date_time.end_serial_time) +
                                                       datetime.timedelta(hours=lag_time), '%H:%M'))

                        # Cross-section width and area
                        trans_prop = Measurement.compute_measurement_properties(meas)
                        n_transects = len(meas.transects)
                        width_list.append(trans_prop['width'][n_transects])
                        area_list.append(trans_prop['area'][n_transects])

                        # Discharge data
                        uh_list.append(0)
                        q_list.append(meas.mean_discharges(meas)['total_mean'])
                        uq_list.append(meas.oursin.u_measurement['total_95'][0])

                # Add height with closest date if Bareme import
                if data_brm is not None:
                    date_start = [datetime.datetime.strptime(a + ' ' + b, '%Y%m%d %H:%M') for a, b in
                                  zip(date_day_list,
                                      date_hour_start)]
                    date_end = [datetime.datetime.strptime(a + ' ' + b, '%Y%m%d %H:%M') for a, b in
                                zip(date_day_list,
                                    date_hour_end)]
                    # Compute middle date of gauging
                    date_mid = [date_start[i] + (date_end[i] - date_start[i]) / 2 for i in
                                range(len(date_start))]
                    # Get the closest date and its difference
                    closest_date = []
                    time_diff = []
                    for date in date_mid:
                        closest_date.append(min(data_brm['time_mid'], key=lambda x: abs(x - date)))
                        time_diff.append(abs(closest_date[-1] - date))
                    id_delta = [i for i in range(len(time_diff)) if time_diff[i] >
                                datetime.timedelta(hours=max(1, abs(delta_time)))]
                    for index in id_delta:
                        closest_date[index] = None
                    # Find if duplicate in selected date
                    set_duplicate = set(
                        [x for x in closest_date if closest_date.count(x) > 1 and x is not None])
                    # Keep the closest date, remove other
                    for date in set_duplicate:
                        id_duplicate = [i for i, e in enumerate(closest_date) if e == date]
                        diff_duplicate = [time_diff[i] for i in id_duplicate]
                        id_min_diff = diff_duplicate.index(min(diff_duplicate))
                        del id_duplicate[id_min_diff]
                        for index in id_duplicate:
                            closest_date[index] = None

                # Set up name of the parent folder or station name
                if save_as_dat:
                    self.name_folder = name_station
                pattern = r'[' + string.punctuation + ']'
                self.name_folder = re.sub(pattern, '', self.name_folder)

                # Save as csv (BaRatinAGE)
                if save_as_csv:
                    print('save as .csv')
                    if data_brm is not None:
                        h_csv = []
                        for i in range(len(closest_date)):
                            if closest_date[i] is None:
                                h_csv.append(-999)
                            else:
                                h_csv.append(float(data_brm.loc[
                                                       data_brm['time_mid'] == closest_date[i], 'cote'].values[
                                                       0]) / 1000)
                    else:
                        h_csv = [-999] * len(q_list)
                    csv_q = [np.round(i, 2) for i in q_list]
                    csv_uq = [np.round(i, 2) for i in uq_list]
                    df_csv = pd.DataFrame({'H': h_csv, 'uH': uh_list, 'Q': csv_q, 'uQ': csv_uq,
                                           'Active': 1})
                    df_csv.to_csv(self.path_folder + '\\' + self.name_folder + '_QRAME_BaRatin.csv', sep=';',
                                  index=False)

                # Save as bad (BaRatinAGE)
                if save_as_bad:
                    print('save as .bad')
                    if data_brm is not None:
                        h_bad = []
                        for i in range(len(closest_date)):
                            if closest_date[i] is None:
                                h_bad.append(-999)
                            else:
                                h_bad.append(float(data_brm.loc[
                                                       data_brm['time_mid'] == closest_date[i], 'cote'].values[
                                                       0]) / 1000)
                    else:
                        h_bad = [-999] * len(q_list)
                    # Absolute uncertainty 68%
                    bad_uq = []
                    for i in range(len(uq_list)):
                        bad_uq.append(q_list[i] * uq_list[i] / 200)
                    # bad_q = [np.round(i, 3) for i in q_list]
                    df_bad = pd.DataFrame(
                        {'H': h_bad, 'uH': uh_list, 'Q': q_list, 'uQ': bad_uq})
                    df_bad.to_csv(self.path_folder + '\\' + self.name_folder + '_QRAME_BaRatin.BAD', sep=' ',
                                  index=False)

                # Save as dat (Barème)
                if save_as_dat:
                    print('save as .dat')
                    if data_brm is not None:
                        j = -1
                        mean_vel = [a / b for a, b in zip(q_list, area_list)]
                        q_list_brm = [np.round(1000 * i) for i in np.round(q_list, 2)]
                        # Update Barème import
                        for i in range(len(closest_date)):
                            if closest_date[i] is not None:
                                comment = data_brm.loc[
                                    data_brm['time_mid'] == closest_date[i], 'commentaire'].values[0]
                                if len(comment) > 0:
                                    comment += f', QRAME update [Transect {current_checked[i]}/{default_checked[i]}, ' \
                                               f'Extrap {extrap_str}{exp_str}]'
                                else:
                                    comment = f'QRAME update [Transect {current_checked[i]}/{default_checked[i]}, ' \
                                              f'Extrap {extrap_str}{exp_str}]'

                                data_brm.loc[data_brm['time_mid'] == closest_date[i],
                                             ['debit', 'incertitude', 'sect_mouillee', 'larg_miroir',
                                              'vitesse_moyenne', 'commentaire']] = \
                                    [q_list_brm[i], np.round(uq_list[i], 2), np.round(area_list[i], 2),
                                     np.round(width_list[i], 2), np.round(mean_vel[i], 2), comment]

                            else:
                                comment = f'QRAME add [Transect {current_checked[i]}/{default_checked[i]}, ' \
                                          f'Extrap {extrap_str}{exp_str}]'
                                data_brm.loc[j] = ['C', 'JGG', name_station, -999, date_day_list[i],
                                                   date_hour_start[i], date_hour_end[i], -32000, '', '', '',
                                                   q_list_brm[i], np.round(uq_list[i], 2), '',
                                                   np.round(area_list[i], 2), '', np.round(width_list[i], 2),
                                                   np.round(mean_vel[i], 2), '', '', '', comment, 'PC', '', '',
                                                   '']
                                j -= 1

                        # Update num for new gauging
                        unmatch_index = data_brm.index[data_brm['num'] == -999].tolist()
                        unmatch_row = data_brm.loc[unmatch_index].sort_values(by=['date', 'start'])
                        len_unmatch = len(str(len(unmatch_index)))
                        zero = '0'
                        list_unmatch = [f'Q{zero * (len_unmatch - len(str(i)))}{i}' for i in
                                        range(1, len(unmatch_index) + 1)]
                        unmatch_row['num'] = list_unmatch
                        unmatch_row.index = unmatch_index
                        data_brm.loc[unmatch_index] = unmatch_row

                        # Delete time columns
                        data_brm.drop(['time_start', 'time_end', 'time_mid'], axis=1, inplace=True)

                        # self.width_list = width_list
                        # self.area_list = area_list
                        #
                        # self.q_list = q_list
                        # self.uq_list = uq_list

                        if j < -1:
                            tk.messagebox.showinfo(title='New gauging added',
                                                   message='Some gaugings were not in the database, they have been '
                                                           'added.')
                    else:
                        # Create dataframe for Barème if no import
                        length = len(q_list)
                        range_length = [f'QRAME{i}' for i in np.arange(1, length + 1)]
                        mean_vel = [a / b for a, b in zip(q_list, area_list)]
                        q_list_brm = [np.round(1000 * i) for i in np.round(q_list, 2)]
                        comment = []
                        for i in range(len(q_list_brm)):
                            comment.append(f'QRAME add [Transect {current_checked[i]}/{default_checked[i]}, ' \
                                           f'Extrap {extrap_str}{exp_str}]')
                        self.comment = comment

                        data_brm = pd.DataFrame(
                            {'C': ['C'] * length, 'JGG': ['JGG'] * length, 'station_name': [self.name_folder] * length,
                             'num': [''] * length, 'date': date_day_list, 'date_start': date_hour_start,
                             'date_end': date_hour_end, 'cote': [-32000] * length, 'cote_start': [''] * length,
                             'cote_end': [''] * length, 'code_station': [''] * length,
                             'debit': q_list_brm, 'incertitude': np.round(uq_list, 2),
                             'distance_station': [''] * length, 'sect_mouillee': np.round(area_list, 2),
                             'peri_mouille': [''] * length, 'larg_miroir': np.round(width_list, 2),
                             'vitesse_moyenne': np.round(mean_vel, 2), 'vitesse_maxi': [''] * length,
                             'vitesse_maxi_surface': [''] * length, 'deniv': [''] * length,
                             'commentaire': comment, 'mode': ['PC'] * length
                             })
                        data_brm = data_brm.sort_values(by=['date', 'date_start'])
                        data_brm = data_brm.reset_index(drop=True)
                        data_brm['num'] = range_length

                    length = len(data_brm)
                    print(f"AREA : {np.round(area_list, 2)}")
                    df = data_brm.to_csv(header=None, index=False, sep=';').strip('\r\n').split('\n')
                    df_str = [i.strip('\r') for i in df]
                    df_bareme = ';\n'.join(df_str)
                    print(self.path_folder + '\\' + self.name_folder + '_QRAME_HYDRO2.dat')
                    with open(self.path_folder + '\\' + self.name_folder + '_QRAME_HYDRO2.dat', "w") as myfile:
                        myfile.write("DEC;  6 13 \nDEB;BA-HYDRO;;;;;;;\n")
                        myfile.write(df_bareme)
                        myfile.write(f';\nFIN;BA-HYDRO;{length + 3};')

                    print(".dat saved")

                # if save_general:
                #     df = pd.DataFrame(
                #         {'station_name': [code_station] * length, 'date_start': date_hour_start,
                #          'date_end': date_hour_end, 'discharge': q_list, 'uncertainty': uq_list,
                #          'area': area_list, 'width': width_list,
                #          'mean_velocity': [''] * length, 'max_velocity': [''] * length
                #          })
            if len(error_meas) > 0:
                print('errors in measurements')
                error_string = ', '.join(error_meas)
                tk.messagebox.showwarning(title='Measurement error',
                                          message=f'QRevInt failed to open measurements : {error_string}')
            # Finish progress bar
            self.value_label['text'] = self.update_progress_label(run=False)
            # Enable button
            self.disable_all(way=False)

        except Exception as e:
            tk.messagebox.showerror(title='Extraction error',
                                    message=f'An error has occured, please contact blaise.calmel@inrae.fr\n'
                                            f'Message error : {e}')
            print('exception')
            # Enable button
            self.disable_all(way=False)
            return


if __name__ == '__main__':
    app = Qrame()
